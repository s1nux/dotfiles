# include config by peer
CFG=./$(shell hostname) ##
include $(CFG) ##

I3DIR=~/.config/i3/
POLYBARCFG=i3/.config/polybar/config.ini ##
BINDDIR=~/.local/bin
MARKDIR=~/.marks
SSHCFG=.ssh/config ##
P10KCFG=zsh/.p10k.zsh ##
HOSTNAME:=$(shell hostname)

all: bin git marks screen vim tmux

bin: $(BINDDIR) ## static binaries
$(BINDDIR):
	@stow bin
	@echo -e "Installed binaries"

git: ~/.gitconfig ## screen cfg
~/.gitconfig:
	@make -C git
	@stow git
	@echo -e "Installed git config"

i3: $(I3DIR) ## make i3 config
$(I3DIR):
	@make -C i3
	@echo -e "Installed i3 config"
	#@stow i3

marks: $(MARKDIR)
$(MARKDIR):
	@stow marks
	@echo -e "Installed mark links"

screen: ~/.screenrc
~/.screenrc:
	@stow screen
	@echo -e "Installed screen config"

ssh: ~/$(SSHCFG) ## make ssh config
	@make -C ssh
	@echo -e "Installed ssh config"
	#@stow ssh

vim: ~/.vimrc
~/.vimrc:
	@stow vim
	@echo -e "Installed vim config"

tmux: ~/.tmux.conf
~/.tmux.conf:
	@stow tmux
	@echo -e "Installed tmux config"

zsh: ~/.zshrc
	@stow zsh
	@echo -e "Installed zsh config"


.PHONY: clean
clean:
	stow -D bin
	stow -D git
	@make -C git clean
	stow -D marks
	stow -D screen
	stow -D vim
	stow -D tmux
#	stow -D zsh
#	stow -D ssh
#	@make -C ssh clean
#	stow -D i3
#	@make -C i3

PHONY: help ## this help
help:
	    @grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

