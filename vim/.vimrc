set nocompatible
filetype off
let need_to_install_plugins=0

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif


call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
" Plug 'junegunn/vim-easy-align'
" Any valid git URL is allowed
" Plug 'https://github.com/junegunn/vim-github-dashboard.git'
" Multiple Plug commands can be written in a single line using | separators
" Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'


" Track the engine.
Plug 'SirVer/ultisnips'
Plug 'jayli/vim-easycomplete'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'


" Themes
Plug 'rafi/awesome-vim-colorschemes'
Plug 'catppuccin/vim', { 'as': 'catppuccin' }
Plug 'miyakogi/conoline.vim'

" Doge python docstring
Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }
Plug 'sansyrox/vim-python-virtualenv'

" Comment/Uncomment
Plug 'preservim/nerdcommenter'

" Pylint
Plug 'vim-syntastic/syntastic'
Plug 'Vimjas/vim-python-pep8-indent'

" ctags
Plug 'craigemery/vim-autotag' 

" Ansible yaml
Plug 'pearofducks/ansible-vim'


" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'preservim/tagbar', { 'on':  'TagbarToggle' }

" Initialize plugin system
call plug#end()
 
" You can revert the settings after the call like so:
"   filetype indent off   " Disable file-type-specific indentation
"   syntax off            " Disable syntax highlighting

filetype plugin indent on
syntax on
set modeline
set modelines=3
set path+=**

" turn on line numbering
set number
" set rnu " relativenumber

" terminal colors
set termguicolors

" sane editing
set tabstop=4
set shiftwidth=4
set softtabstop=4
set colorcolumn=80
set expandtab

" color scheme
syntax on
colorscheme catppuccin_mocha " catppuccin-latte, catppuccin-frappe, catppuccin-macchiato, catppuccin-mocha
" highlight current line
let g:conoline_auto_enable = 1
let g:conoline_color_normal_dark = 'guibg=#333333 guifg=#dddddd'
let g:conoline_color_insert_dark = 'guibg=black guifg=white'
set background=dark


filetype on
filetype plugin indent on
" Enable mouse use in all modes
set mouse=a

" move through split windows
nmap <leader><Up> :wincmd k<CR>
nmap <leader><Down> :wincmd j<CR>
nmap <leader><Left> :wincmd h<CR>
nmap <leader><Right> :wincmd l<CR>

" close all splits except current one
nmap <leader>x :only<CR>

" move through buffers
nmap <leader>[ :bp!<CR>
nmap <leader>] :bn!<CR>


"------------------------------------------------------------------------------
" Nerdtree for vim
"------------------------------------------------------------------------------
nmap <C-S-e> :NERDTreeToggle<CR>
let NERDTreeIgnore=['\.pyc$', '__pycache__', '\.sqlite$', '\.sqlite3$']
" Nerdtree commentary
let g:NERDCreateDefaultMappings = 1
let g:NERDSpaceDelims = 1
let g:NERDToggleCheckAllLines = 1

"------------------------------------------------------------------------------
" TagBar for vim
"------------------------------------------------------------------------------
nmap <C-S-t> :TagbarToggle<CR>
let g:autotagTagsFile=".tags"

"------------------------------------------------------------------------------
" Ultisnip for vim
"------------------------------------------------------------------------------
" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
" - https://github.com/Valloric/YouCompleteMe
" - https://github.com/nvim-lua/completion-nvim
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

noremap gr :EasyCompleteReference<CR>
noremap gd :EasyCompleteGotoDefinition<CR>
noremap rn :EasyCompleteRename<CR>
noremap gb :BackToOriginalBuffer<CR>
nnoremap <C-_>  <plug>NERDCommenterToggle<CR>


let g:syntastic_python_checkers = ['pylint']

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1

let g:python3_host_prog='/usr/bin/python3'

augroup ansible_vim_fthosts
  autocmd!
  autocmd BufNewFile,BufRead hosts setfiletype yaml.ansible
augroup END
let g:ansible_unindent_after_newline = 1
let g:ansible_name_highlight = 'd'
let g:ansible_extra_keywords_highlight = 1


" setlocal spell spelllang=fr
set spelllang=fr
augroup ansible_vim_fthosts
  autocmd!
  autocmd FileType latex,txt,org,tex,md,markdown setlocal spell
"  hi clear SpellBad
"  hi SpellBad cterm=underline guisp=Red
"  hi SpellBad gui=undercurl
  hi SpellBad   guisp=red    gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
  hi SpellCap   guisp=yellow gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
  hi SpellRare  guisp=blue   gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
  hi SpellLocal guisp=orange gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
augroup END

" hi SpellBad   guisp=red    gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
" hi SpellCap   guisp=yellow gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
" hi SpellRare  guisp=blue   gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
" hi SpellLocal guisp=orange gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
" 
