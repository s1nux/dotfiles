# Customize to your needs...
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:~/.local/bin/

export PATH=$PATH:$(go env GOPATH)/bin
export GOPATH=$(go env GOPATH)

export XDG_CONFIG_HOME="$HOME/.config"
export DOOMDIR=$XDG_CONFIG_HOME/doom

export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

export LYNX_CFG=~/.lynxrc
export OPENSCADATH=/usr/lib:~/.local/share/OpenSCAD/libraries:/usr/share/openscad/libraries

function encrypt-file {  openssl aes-256-cbc -in $1 -out $1.enc && rm -i $1; }
function decrypt-file {  openssl aes-256-cbc -d -in $1 -out ${1%.enc} ; }
export TEXINPUTS=.:$HOME/.dotfiles/latex/.latex:/usr/share/texmf/tex/latex/:/usr/local/share/texmf/latex/:

export SWT_GTK3=0
export BC_ENV_ARGS=~/.bc
export EDITOR=/usr/bin/vim

export VISUAL="vim"
export TERM="xterm-256color"

