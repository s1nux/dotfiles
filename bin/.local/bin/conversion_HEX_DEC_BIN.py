#!/usr/bin/python

import getopt, sys


if sys.argv[0].__str__()[-3:] == "h2d":
    try:
        h = hex(int(sys.argv[1].__str__(), 0))
    except:
        print("\nUsage : "+sys.argv[0]+" [int|hex]\n")

        exit(1)

    print("decimal = "+int(sys.argv[1].__str__(), 0).__str__())
    print("hex     = "+sys.argv[1].__str__())
    print("bin     = "+bin(int(sys.argv[1].__str__(), 0)).__str__())

if sys.argv[0].__str__()[-3:] == "d2h":
    
    try:
        i = int(sys.argv[1])
    except:
        print("\nUsage : "+sys.argv[0]+" [int|hex]\n")

        exit(1)

    print("decimal = "+sys.argv[1].__str__())
    print("hex     = "+hex(int(sys.argv[1])).__str__())
    print("bin     = "+bin(int(sys.argv[1])).__str__())

