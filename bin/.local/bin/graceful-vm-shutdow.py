#!/usr/bin/env python

import json
from pathlib import Path
from os import path
import socket
import argparse

#def qemu_socket_monitor(vm_id: str) -> path:
#    return f'/tmp/qga{vm_id}.sock'

def execute(vm: str, command: str):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.connect(vm)
    json_line = json.dumps({'execute': command}) + '\n'
    sock.sendall(json_line.encode('utf-8'))
#    f = sock.makefile('rw')
#    return json.loads(f.readline())

def main():
    parser = argparse.ArgumentParser(description='graceful shutdown VM.')
    parser.add_argument('vm', help='vm socket to parse', type=str)
    args = parser.parse_args()
#    print(qemu_socket_monitor(args.vm))
    execute(args.vm, "guest-shutdown")
    return 0

if __name__ == '__main__':
    exit(main())
