#!/bin/bash
    
if [ ${0##*/} == "single.sh" ] ; then

case `hostname` in 
        "t480")
            xrandr --output HDMI2 --off --output HDMI1 --off --output DP1 --off --output eDP1 --primary --mode 2560x1440 --pos 0x0 --rotate normal --output DP2 --off ;;
        "x250")
            xrandr --output HDMI-1 --off --output HDMI-2 --off --output DP-1 --off --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-2 --off ;;
        "ITI-sinux-A402")
            xrandr --output DP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-2 --off
esac

fi

if [ ${0##*/} == "dual.sh" ] ; then

case `hostname` in 
        "t480")
            xrandr --output HDMI2 --mode 1920x1200 --pos 2560x0 --rotate normal --output HDMI1 --off --output DP1 --off --output eDP1 --primary --mode 2560x1440 --pos 0x216 --rotate normal --output DP2 --off;;
        "x250")
            xrandr --output HDMI-1 --mode 1920x1200 --pos 0x0 --rotate normal --output HDMI-2 --off --output DP-2 --off --output eDP-1 --primary --mode 1920x1080 --pos 1920x432 --rotate normal --output DP-2 --off;;
        "ITI-sinux-A402")
            xrandr --output DP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-2 --mode 1920x1200 --pos 1920x0 --rotate normal
esac

fi


if [ ${0##*/} == "right.sh" ] ; then

case `hostname` in 
        "t480")
            xrandr --output HDMI-2 --mode 1920x1200 --pos 2560x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output eDP-1 --primary --mode 2560x1440 --pos 0x216 --rotate normal --output DP-2 --off;;
        "x250")
            xrandr --output HDMI-1 --mode 1920x1200 --pos 0x0 --rotate normal --output HDMI-2 --off --output DP-2 --off --output eDP-1 --primary --mode 1920x1080 --pos 1920x432 --rotate normal --output DP-2 --off;;
        "ITI-sinux-A402")
            xrandr --output DP-1 --off --output DP-2 --primary --mode 1920x1200 --pos 0x0 --rotate normal
esac

fi

if [ ${0##*/} == "left.sh" ] ; then

case `hostname` in 
        "t480")
            xrandr --output HDMI-2 --mode 1920x1200 --pos 2560x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output eDP-1 --primary --mode 2560x1440 --pos 0x216 --rotate normal --output DP-2 --off;;
        "x250")
            xrandr --output HDMI-1 --mode 1920x1200 --pos 0x0 --rotate normal --output HDMI-2 --off --output DP-2 --off --output eDP-1 --primary --mode 1920x1080 --pos 1920x432 --rotate normal --output DP-2 --off;;
        "ITI-sinux-A402")
            xrandr --output DP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-2 --off
esac

fi
